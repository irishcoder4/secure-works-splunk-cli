
"""A basic command line utility for searches on the Splunk REST API."""
import sys, os
from time import sleep
import splunklib.client as client
from splunklib.binding import HTTPError
import splunklib.results as results
import argparse

class SplunkCLI:
    """ The Splunk CLI class is designed to provide easy searches through the command line """
    def __init__(self):
        """ Create the splunk service from command line args"""
        parser = argparse.ArgumentParser(description='Extract data from Splunk using the REST API.')
        parser.add_argument('--host', action="store", dest="host", required=True, help='splunk host (reqd)')
        parser.add_argument('--port', action="store", dest="port", required=True, help='splunk rest port (reqd)')
        parser.add_argument('--username', action="store", dest="username", required=True, help='splunk username (reqd)')
        parser.add_argument('--password', action="store", dest="password", required=True, help='splunk password (reqd)')
        parser.add_argument('--search', action="store", dest="search", help='splunk search (optional)')

        self.args = parser.parse_args()
        self.search_txt = self.args.search
        try:
            self.service = client.connect(
                    host=self.args.host,
                    port=self.args.port,
                    username=self.args.username,
                    password=self.args.password
                    )
        except Exception as e:
            print str(e)
            exit(0)

    def search(self, search):
        """ Performs the splunk search"""
        verbose = 0
        try:
            self.service.parse(search, parse_only=True)
        except HTTPError as e:
            raise Exception("query '%s' is invalid:  \n\t%s" % (search,
                e.message), 2)
        job = self.service.jobs.create(search, **{"exec_mode": "blocking"})
        while True:
            while not job.is_ready():
                pass
            stats = {'isDone': job['isDone'],
                    'doneProgress': job['doneProgress'],
                    'scanCount': job['scanCount'],
                    'eventCount': job['eventCount'],
                    'resultCount': job['resultCount']}
            progress = float(stats['doneProgress'])*100
            scanned = int(stats['scanCount'])
            matched = int(stats['eventCount'])
            output = int(stats['resultCount'])
            status = ("\r%03.1f%% | %d scanned | %d matched | %d results \n\n"
                    % (progress, scanned, matched, output))
            sys.stdout.write(status)
            sys.stdout.flush()
            if stats['isDone'] == '1':
                if verbose > 0: sys.stdout.write('\n')
                break
            sleep(2)
        search_results = job.results(**{"output_mode": "csv"})
        print(search_results.read())
        sys.stdout.write('\n')
        job.cancel()

if __name__ == '__main__':
    """ Creates the SplunkCLI object and pulls the input for the search"""
    splunk_cli = SplunkCLI()
    """ If search came from command line option, do the search and quit. """
    if splunk_cli.search_txt is not None:
        splunk_cli.search("search " + splunk_cli.search_txt)
        exit(0)

    """ If search didn't, let's provide a way to input a search """
    search = ""
    while search != "end":
        search = raw_input("Please type in your single line splunk search (type 'end' to exit, return or enter ends the search): ")
        if search == "end":
            exit(0)
        else:
            splunk_cli.search("search " + search)
