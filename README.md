# The Splunk CLI for Secureworks Interview - Built in Python

>  **Advisory** : Part of this text and code is being referenced from the Splunk examples in the [Splunk python sdk](https://github.com/splunk/splunk-sdk-python)

#### Version 0.1

<br><br>
## 1.0 Prequisites (Must have external Splunk instance with data installed)

#### A. Install a free evaluation version of [Splunk:](https://www.splunk.com/en_us/download/get-started-with-your-free-trial.html)
* As an alternative you could use docker and the [splunk image](https://hub.docker.com/r/splunk/splunk/). `docker pull splunk/splunk:7.0.0`

#### B. Upload the tutorial dataset to your Splunk installation:

* [Download] (http://docs.splunk.com/Documentation/Splunk/7.0.2/SearchTutorial/Systemrequirements#Download_the_tutorial_data_files) the tutorial data files
* [Get the data into Splunk] (http://docs.splunk.com/Documentation/Splunk/latest/SearchTutorial/GetthetutorialdataintoSplunk)

<br><br>
## 2.0 Docker Install (Experimental)


#### A. Install docker
* Find your platform and install from these
  [directions](https://docs.docker.com/install/)

#### B. Git Clone Project
    git clone git@gitlab.com:irishcoder4/secure-works-splunk-cli.git

#### C. Docker Build
    docker build -t splunk-cli .
    docker images
    docker run -it <IMAGE ID> /bin/sh

<br><br>
## 3.0 Manual Install (More tested and is an option if you don't feel comfortable with Docker)

#### A. Install Python 2.6+

The Splunk SDK for Python requires [Python 2.6+](https://www.python.org/downloads/).

#### B. Install Python Pip
[Instructions](https://pip.pypa.io/en/stable/installing/)

#### C. Git Clone Project
    git clone git@gitlab.com:irishcoder4/secure-works-splunk-cli.git


#### D. Install the Requirements

Here's what you need to get going with the SplunkCLI for Python.

  `[sudo] pip install -r requirements.txt`

  To run the examples, you must put the root of
  the SDK on your PYTHONPATH. For example, if you have downloaded the code to your
  home folder and are running OS X or Linux, add the following line to your
  **.bash_profile**:

      export PYTHONPATH=examples/splunk-sdk-python:.
<br><br>


###### D1. Install the Splunk SDK for Python and Virtualenv if there are issues
Get the Splunk SDK for Python; [download the SDK as a ZIP](http://dev.splunk.com/view/SP-CAAAEBB)
and extract the files. Or, if you want to contribute to the SDK, clone the
repository from [GitHub](https://github.com/splunk/splunk-sdk-python).

## 4.0 Run SplunkCLI utility
Using Python, I have written a command line tool that interfaces with the Splunk
REST API. The tool should have the following command line arguments:

* Splunk REST Server URL
* Splunk Username
* Splunk Password
* Splunk Query

The SplunkCLI requires a common set of arguments
that specify the Splunk host, port, and login credentials. For a
full list of command-line arguments, include `--help` as an argument to any of
the examples.

`Be careful with the python command.  Verify it is python version 2 by typing
python --version`

#### A. Run without search in command
    python splunk-cli.py --host "localhost" --port 8089 --username "admin"  --password "changeme"

#### B. Run with search in command
    python splunk-cli.py --host "localhost" --port 8089 --username "admin"  --password "changeme" --search "search  index="test123" useragent="Googlebot/2.1*" | dedup referer_domain | table referer_domain"

##### Utility will output
    Please type in your single line splunk search (type 'end' to exit, return or enter ends the search):

Use utility as needed.

<br><br><br>
## 5.0 Requested Queries

###### Please run unit tests to make sure your install went Ok.  (Apologies.  These tests only work on a system with Bash.  Future improvement would be pure python) 
    chmod +x unit-tests.sh; ./unit-tests.sh


`Assuming the index name is "test123".  When doing your search, replace the index with your index.`

#### A. Generate a table consisting of [Timestamp, Client IP, URL] where the HTTP Server’s response was a 50X error code.

    index=test123 status=5* | eval url=referer_domain.uri | table req_time, clientip, url

#### B. Generate a list of web pages that have been indexed by Google (“Googlebot”)

    index="test123" useragent="Googlebot/2.1*" | dedup referer_domain | table referer_domain

#### C. Generate a list of usernames that were used in failed SSH login attempts from 27.0.0.0/8
    'index=test123 sourcetype=secure _raw="*Failed password*" _raw="*ssh*" | rex "^.* Failed password for (invalid user)?\s?(?<user>\w+)\sfrom\s?(?<ipAddr>.*)\sport"| eval user_ns=trim(user) | dedup user_ns | search ipAddr="27.0.0.0/8" | table user_ns, ipAddr'
