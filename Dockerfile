FROM python:2.7-alpine
MAINTAINER Jared Laney <jared@kepora.com>

#Create Env variables
ENV APPLICATION splunk_cli
ENV PROJECT_DIR /var/apps/$APPLICATION/

#Create Application structure and add it
RUN mkdir -p $PROJECT_DIR $APP_CONFIG_DIR
ADD . $PROJECT_DIR
WORKDIR $PROJECT_DIR

#Install python deps
RUN pip install -r requirements.txt

CMD [/bin/sh]
