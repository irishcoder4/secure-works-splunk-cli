echo "Checking for prerequisites on your system..."

#Checking for git
if command -v git &>/dev/null; then
    echo git is installed.  Requirement Satisfied
else
    echo git is not installed, please install git and try again.
    exit
fi

#Checking for python2
if command -v python &>/dev/null; then
    echo Python is installed.  Requirement Satisfied
else
    echo Python is not installed, please install python2 and try again.
    exit
fi

#Checking for pip
if command -v pip &>/dev/null; then
    echo pip is installed.  Requirement Satisfied
else
    echo pip is not installed, please install pip and try again.
    exit
fi

#checking for virtualenv.
if command -v virtualenv &>/dev/null; then
    echo Virtualenv is installed.  Requirement Satisfied
else
    echo Virtualenv is not installed, please go through manual instructions.
    exit
fi

python splunk-cli.py --host "localhost" --port 8089 --username "admin"  --password "changeme" --search "index="test123" useragent="Googlebot/2.1*" | dedup referer_domain | table referer_domain"
if [ $? -eq 0 ]; then
    echo PASS TEST 1
else
    echo FAIL
fi

python splunk-cli.py --host "localhost" --port 8089 --username "admin"  --password "changeme" --search "index=test123 status=5* | eval url=referer_domain.uri | table req_time, clientip, url"
if [ $? -eq 0 ]; then
    echo PASS TEST 2
else
    echo FAIL
fi

python splunk-cli.py --host "localhost" --port 8089 --username "admin"  --password "changeme" --search 'index=test123 sourcetype=secure _raw="*Failed password*" _raw="*ssh*" | rex "^.* Failed password for (invalid user)?\s?(?<user>\w+)\sfrom\s?(?<ipAddr>.*)\sport"| eval user_ns=trim(user) | dedup user_ns | search ipAddr="27.0.0.0/8" | table user_ns, ipAddr'
if [ $? -eq 0 ]; then
    echo PASS TEST 3
else
    echo FAIL
fi

echo "SplunkCLI has been successfully installed"

echo "Here is a sample command: python2.7 splunk-cli.py --host "localhost" --port 8089 --username "admin"  --password "changeme""
